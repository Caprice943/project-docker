-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost
-- Généré le : jeu. 26 mai 2022 à 14:30
-- Version du serveur : 10.4.21-MariaDB
-- Version de PHP : 8.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `docker`
--

-- --------------------------------------------------------

--
-- Structure de la table `recette`
--

CREATE TABLE `recette` (
  `id` int(13) NOT NULL,
  `nom` varchar(55) NOT NULL,
  `description` text NOT NULL,
  `ingredients` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `recette`
--

INSERT INTO `recette` (`id`, `nom`, `description`, `ingredients`) VALUES
(1, 'recette brownie', 'realiser un brownie fondant que toute la famille va adorer', '150g beurre 250g chocolat'),
(3, 'Omelette garnie', 'Omelette a la poele', '4 oeufs 2 tomates sel poivre jambon'),
(4, 'Fondue au poireau', 'Délicieuse et facile fondue au poireau', '600g poireau creme fraiche sel poivre beurre'),
(5, 'Quiche lorraine', 'Délicieuse quiche lorraine', 'pate brisee creme fraiche sel poivre lardon beurre'),
(6, 'pate à la carbonara', 'pate à la carbonara à la française', '500g pate 3jaunes oeufs sel poivre 50cl creme fraiche 500g lardon'),
(7, 'crepes', 'crepes faites maison', 'farine beurre lait oeufs');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `recette`
--
ALTER TABLE `recette`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `recette`
--
ALTER TABLE `recette`
  MODIFY `id` int(13) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
