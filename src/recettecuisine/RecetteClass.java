/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package recettecuisine;

/**
 *
 * @author capricendombimouiri
 */
public class RecetteClass {
    public int id;
    public String nom;
    public String description;
    public String ingredient;
    
    
    public RecetteClass( int id, String nom, String description, String ingredient){
        this.id = id;
        this.nom = nom;
        this.description = description;
        this.ingredient = ingredient;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIngredient() {
        return ingredient;
    }

    public void setIngredient(String ingredient) {
        this.ingredient = ingredient;
    }

    @Override
    public String toString() {
        return "RecetteClass{" + "id=" + id + ", nom=" + nom + ", description=" + description + ", ingredient=" + ingredient + '}';
    }
    
    
    
}
