/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */

        
import java.util.Scanner;
import java.sql.*;

/**
 *
 * @author capricendombimouiri
 */
public class RecetteCuisine { 
    
    //Class.forName("com.mysql.cj.jdbc.Driver");
    //Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/docker","caprice","caprice22"); 

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
   
           System.out.println("\n\n***** MySQL JDBC Connection Testing *****");
		   Connection conn = null;
                   
           try
           {
               //Chargement du driver
               Class.forName ("com.mysql.cj.jdbc.Driver");
               String userName = "caprice";
               String password = "caprice22";
               String url = "jdbc:MySQL://localhost:3306/docker";
               
               //Connexion à la base de donnée
               conn = DriverManager.getConnection(url, userName, password);
               System.out.println ("\nDatabase Connection Established...");
               
               //État de connexion
               Statement st = conn.createStatement();
               
              
                Scanner sc = new Scanner(System.in);
                int choice;
                //Le menu se réaffiche après chaque choix pour l'arrêter entrer 0
                do{
                    System.out.println("-------------MENU--------------------");
                    System.out.println("1-Ajouter une recette");
                    System.out.println("2-Liste des recettes");
                    System.out.println("3-Modifier une recette");
                    System.out.println("4-Supprimer une recette");
                    System.out.println("5-Rechercher une recette par mot clé");
                    System.out.println("Choississez une option dans le menu ci-dessus :");
                    choice = sc.nextInt();
              
                    switch(choice){
                        case 1:
                            System.out.println("-----Veuillez saisir les données suivantes--------");
                            System.out.println("Le nom de votre recette : ");
                            sc.nextLine();
                            String nomEnter = sc.nextLine();
                            System.out.println("La description de votre recette : ");
                            String descEnter = sc.nextLine();
                            System.out.println("Les ingrédients de votre recette : ");
                            String ingrEnter = sc.nextLine();
                            
                            
                             //Creation de la requete d'insertion
                            //String query = "INSERT INTO recette(nom, description, ingredients) VALUES('" + nomEnter + "','" + descEnter + "','" + ingrEnter + "')";
                            String query1 = "INSERT INTO recette(nom, description, ingredients) VALUES(?,?,?)";
                            PreparedStatement pstmt = conn.prepareStatement(query1);
                            pstmt.setObject(1,nomEnter );
                            pstmt.setObject(2,descEnter );
                            pstmt.setObject(3,ingrEnter );
                            pstmt.executeUpdate();
                            //pstmt.executeUpdate(query);
                            break;
                            
                        case 2:
                            
                              //Creation de la requete de selection
                              ResultSet res = st.executeQuery("SELECT * FROM recette");
               
                            //Parcours des données
                            while(res.next()){
                            System.out.println("----------------------------------------------");
                            System.out.println("Id de la recette : " +res.getString(1));
                            System.out.println("--Nom de la recette : " + res.getString(2));
                            System.out.println("--Description : " + res.getString(3));
                            System.out.println("--Les ingrédients :" + res.getString(4));
                            System.out.println("    ");
                            }
                            break;
                            
                        case 3:
                            System.out.println("Veuillez saisir l'id de la recette : ");
                            int idEnter = sc.nextInt();
                            System.out.println("Le nouveau nom de la recette : ");
                            sc.nextLine();
                            String newNom = sc.nextLine();
                            System.out.println("La nouvelle description de votre recette : ");
                            String newDesc = sc.nextLine();
                            System.out.println("Les nouveaux ingrédients de votre recette : ");
                            String newIngr = sc.nextLine();
                            
                            String query2 = "UPDATE recette SET nom = ?, description = ?, ingredients = ? WHERE id = ? ";
                            PreparedStatement pstm = conn.prepareStatement(query2);
                            pstm.setObject(1,newNom);
                            pstm.setObject(2,newDesc);
                            pstm.setObject(3,newIngr);
                            pstm.setObject(4, idEnter);
                            pstm.executeUpdate();
                            break;
                            
                            
                        case 4:
                            System.out.println("Veuillez saisir l'id de la recette : ");
                            int idEntre = sc.nextInt();
                            
                            String query3 = "DELETE FROM recette WHERE id = ?";
                            PreparedStatement pst = conn.prepareStatement(query3);
                            pst.setObject(1, idEntre);
                            pst.executeUpdate();
                            break;
                            
                            
                        case 5:
                            System.out.println("Le mot clé de la recette que vous recherchez : ");
                            sc.nextLine();
                            String word = sc.nextLine();
                            
                            
                            String query4 ="SELECT * FROM recette WHERE nom LIKE ? ";
                            PreparedStatement ps = conn.prepareStatement(query4);
                            ps.setObject(1, '%' + word + '%');
                           
                           
                            
                             ResultSet resul = ps.executeQuery();
                             while(resul.next()){
                            System.out.println("----------------------------------------------");
                            System.out.println("Id de la recette : " +resul.getString(1));
                            System.out.println("--Nom de la recette : " + resul.getString(2));
                            System.out.println("--Description : " + resul.getString(3));
                            System.out.println("--Les ingrédients :" + resul.getString(4));
                            System.out.println("    ");
                            }
                            break;
                          
                        }
                    } while(choice!=0);
               
              
           }
          catch (Exception ex)
           {
		       System.err.println ("Cannot connect to database server");
			   ex.printStackTrace();
           }   
        
       
    }
    
}
