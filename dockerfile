FROM openjdk:11
WORKDIR /src/recettecuisine
COPY . /src/recettecuisine
RUN ["javac", "src/recettecuisine/RecetteCuisine.java"]
ENTRYPOINT ["java","RecetteCuisine"]

